import { Icon } from "@interface/icon";

export const icons: Icon[] = [
  {
    title: "Personal gitlab",
    name: "Gitlab",
    svg: "assets/svg/gitlab.svg",
    classAnchor: "link",
    href: "https://www.gitlab.com/users/Bangal22",
  },
  {
    title: "Personal linkdin",
    name: "Linkedin",
    svg: "assets/svg/linkedin.svg",
    classAnchor: "link",
    href: "https://es.linkedin.com/in/bangal-camara",
  },
  {
    title: "Personal gmail",
    name: "Gmail",
    svg: "assets/svg/gmail.svg",
    classAnchor: "link",
    href: "https://mail.google.com/mail/u/0/?fs=1&to=bangalcamara2002@gmail.com&tf=cm",
  },
];
