import { NavBar } from "@interface/index";

export const navbar: NavBar[] = [
  {
    title: "Go to projects",
    value: "Home",
    href: "#/projects",
    classAnchor: "link",
  },
  {
    title: "Go to projects",
    value: "Projects",
    href: "#/projects",
    classAnchor: "link",
  },
  {
    title: "Go to experience",
    value: "Experience",
    href: "/experience",
    classAnchor: "link",
  },
  {
    title: "Go to about",
    value: "About",
    href: "#/home",
    classAnchor: "link",
  },
  {
    title: "Go to why me?",
    value: "Why me?",
    href: "#/why-me",
    classAnchor: "link",
  },
];
