export * from "@data/navbar";
export * from "@data/icons";
export * from "@data/main-projects"
export * from "@data/other-projects"
export * from "@data/experience"