import { Project, ProjectType, MediaType } from "@interface/index";
const { Frontend, Backend, DataEngineering, DataVisualization } = ProjectType;
const { video, image } = MediaType;
export const mainProjects: Project[] = [
  {
    title: "Elecciones españolas 2023",
    projectType: [DataEngineering, Frontend, Backend, DataVisualization],
    media: {
      type: image,
      url: "/assets/img/projects-photos/elecciones-espanolas.webp",
      description: "description",
    },
    description:
      "eleccionesespanolas2023.vercel.app is a website that uses state-of-the-art technology to retrieve real-time data and analyze public opinion on political parties. With the objective of providing an objective and updated perspective on political trends.",
    technologies: [
      {
        name: "Azure",
        image: "assets/svg/azure.svg",
      },
      {
        name: "Docker",
        image: "assets/svg/docker.svg",
      },
      {
        name: "PostgreSQL",
        image: "assets/svg/postgresql.svg",
      },
      {
        name: "Express",
        image: "assets/svg/express.svg",
      },
      {
        name: "TypeScript",
        image: "assets/svg/typescript.svg",
      },
      {
        name: "JavaScript",
        image: "assets/svg/javascript.svg",
      },
      {
        name: "Python",
        image: "assets/svg/python.svg",
      },
      {
        name: "Node js",
        image: "assets/svg/nodejs.svg",
      },
      {
        name: "Astro",
        image: "assets/svg/astro.svg",
      },
      {
        name: "Chart js",
        image: "assets/svg/chartjs.svg",
      },
      {
        name: "Railway",
        image: "assets/svg/railway.svg",
      },
      {
        name: "Nifi",
        image: "assets/svg/nifi.svg",
      },
      {
        name: "Azure function",
        image: "assets/svg/azurefunction.svg",
      },
      {
        name: "Azure SQL Server",
        image: "assets/svg/azureSql.svg",
      },
      {
        name: "Power Bi",
        image: "assets/svg/powerbi.svg",
      },
      {
        name: "Azure Data Lake Storage Gen2",
        image: "assets/svg/azuredatalake.svg",
      },
      {
        name: "Vercel",
        image: "assets/svg/vercel.svg",
      },
      {
        name: "Prettier",
        image: "assets/svg/prettier.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #ffae00 0%, #ff2b2b 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/blog-articles",
      repository: "https://eleccionesespanolas2023.vercel.app/",
    },
  },
  {
    title: "Spotilife",
    projectType: [Frontend, Backend],
    media: {
      type: image,
      url: "assets/img/projects-photos/spotilife.webp",
      description: "description",
    },
    description:
      "This project is built first of all to practice the scalable file structure to make the project easier to maintain in the future. Also this application is built to see another way to view spotify data with simple fronted.",
    technologies: [
      {
        name: "Angular",
        image: "assets/svg/angular.svg",
      },
      {
        name: "TypeScript",
        image: "assets/svg/typescript.svg",
      },
      {
        name: "JavaScript",
        image: "assets/svg/javascript.svg",
      },
      {
        name: "Node js",
        image: "assets/svg/nodejs.svg",
      },
      {
        name: "Sass",
        image: "assets/svg/sass.svg",
      },
      {
        name: "Bootstrap",
        image: "assets/svg/bootstrap.svg",
      },
      {
        name: "Cloudflare",
        image: "assets/svg/cloudflare.svg",
      },
      {
        name: "Render",
        image: "assets/svg/render.svg",
      },
      {
        name: "Express",
        image: "assets/svg/express.svg",
      },
      {
        name: "Prettier",
        image: "assets/svg/prettier.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #1bcb59 0%, #499162 100%",
      boxShadow: "0 40px 86px rgba(36, 223, 102, 0.34)",
    },
    urlProjects: {
      web: "https://ae34325e.spotilife.pages.dev/#/",
      repository: "https://gitlab.com/Bangal22/spotiprofile",
    },
  },
  {
    title: "Github-Profile",
    projectType: [Frontend, DataVisualization],
    media: {
      type: image,
      url: "assets/img/projects-photos/github-profile.webp",
      description: "description",
    },
    description:
      "On that website you can search the GitHub profile of any person, showing you the most relevant information and graphs of the current state of the repository.",
    technologies: [
      {
        name: "Sveltekit",
        image: "assets/svg/svelte.svg",
      },
      {
        name: "TypeScript",
        image: "assets/svg/typescript.svg",
      },
      {
        name: "Chart js",
        image: "assets/svg/chartjs.svg",
      },
      {
        name: "Bootstrap",
        image: "assets/svg/bootstrap.svg",
      },
      {
        name: "Prettier",
        image: "assets/svg/prettier.svg",
      },
      {
        name: "Vite",
        image: "assets/svg/vite.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #202D67 0%, #494ED6 100%",
      boxShadow: "0 40px 86px rgba(73, 78, 214, 0.34);",
    },
    urlProjects: {
      web: "https://github-profile-bangal22.vercel.app/",
      repository: "https://gitlab.com/Bangal22/github-profile",
    },
  },
  {
    title: "Secció Àuria",
    projectType: [DataEngineering, Frontend, Backend],
    media: {
      type: image,
      url: "/assets/img/projects-photos/seccio-auria.png",
      description: "description",
    },
    description:
      "eleccionesespanolas2023.vercel.app is a website that uses state-of-the-art technology to retrieve real-time data and analyze public opinion on political parties. With the objective of providing an objective and updated perspective on political trends.",
    technologies: [
      {
        name: "Angular",
        image: "assets/svg/angular.svg",
      },
      {
        name: "TypeScript",
        image: "assets/svg/typescript.svg",
      },
      {
        name: "JavaScript",
        image: "assets/svg/javascript.svg",
      },
      {
        name: "Node js",
        image: "assets/svg/nodejs.svg",
      },
      {
        name: "Sass",
        image: "assets/svg/sass.svg",
      },
      {
        name: "Bootstrap",
        image: "assets/svg/bootstrap.svg",
      },
      {
        name: "Nginx",
        image: "assets/svg/nginx.svg",
      },
      {
        name: "Apache",
        image: "assets/svg/apache.svg",
      },
      {
        name: "MySQL",
        image: "assets/svg/mysql.svg",
      },
      {
        name: "Express",
        image: "assets/svg/express.svg",
      },
      {
        name: "Firebase",
        image: "assets/svg/firebase.svg",
      },
      {
        name: "Json web tokens (JWT)",
        image: "assets/svg/jwt.svg",
      },
      {
        name: "Clouding.io",
        image: "assets/svg/clouding.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #3e5ce0 0%, #8697e2 100%",
      boxShadow: "0 40px 86px rgba(71, 94, 224, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/projecte",
      repository: "https://gitlab.com/Bangal22/projecte",
    },
  },
];
