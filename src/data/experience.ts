import type { Experience } from "@interface/index";

export const experience: Experience[] = [
  {
    name: "Pizza Word",
    location: "Tordera, Catalonia (Spain)",
    position: "Pizza maker and delivery",
    description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
    img: "assets/img/experience/pizza_word.webp",
    responsibilities: [
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus rerum, impedit molestiae maiores corrupti.",
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus rerum, impedit molestiae maiores corrupti.",
    ],

    mainAchievement: [
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus rerum, impedit molestiae maiores corrupti.",
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus rerum, impedit molestiae maiores corrupti.",
    ],
  },
  {
    name: "In2art",
    location: "Girona, Catalonia (Spain)",
    position: "Frontend developer",
    description: "Company internships at IN2ART",
    img: "assets/img/experience/in2art.jpg",
    responsibilities: [
      "Create the comments section of the application from scratch..",
      "Implement reaction emojis in the comments to reply (something similar to whatsapp or instagram has).",
      "Improve user preference in the choice of desired artistic themes.",
    ],

    mainAchievement: [
      "Collaborate with designers and work on the frontend side of the web application, unifying criteria and coordinating development on both sides of the application (frontend and backend).",
      "Participate in the meetings about new implementations that the application could acquire.",
      "I obtained the highest possible grade in the overall evaluation of the internship.",
    ],
  },
].reverse();
