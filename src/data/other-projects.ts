import { Project, ProjectType, MediaType } from "@interface/index";
const {
  Frontend,
  Backend,
  DataEngineering,
  DataVisualization,
  ProgrammingLogic,
  DataScientist,
  MachineLearning,
  Programming,
} = ProjectType;
const { video, image } = MediaType;
export const otherProjects: Project[] = [
  {
    title: "Codewars",
    projectType: [
      DataEngineering,
      Backend,
      DataVisualization,
      ProgrammingLogic,
    ],
    media: {
      type: image,
      url: "/assets/svg/codewars.svg",
      description: "Codewars logo",
    },
    description:
      "Project where you can find programming exercises of diverse levels solved with different languages",
    technologies: [
      {
        name: "Python",
        image: "assets/svg/python.svg",
      },
      {
        name: "Java",
        image: "assets/svg/java.svg",
      },
      {
        name: "JavaScript",
        image: "assets/svg/javascript.svg",
      },
      {
        name: "Rust",
        image: "assets/svg/rust.svg",
      },
      {
        name: "TypeScript",
        image: "assets/svg/typescript.svg",
      },
      {
        name: "Lua",
        image: "assets/svg/lua.svg",
      },
      {
        name: "D",
        image: "assets/svg/d.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #ff4d4d 0%, #f96e5d 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://www.codewars.com/users/Bangal",
      repository: "https://gitlab.com/Bangal22/codewars",
    },
  },
  {
    title: "Rust machine learning algorithms ",
    projectType: [DataScientist, MachineLearning],
    media: {
      type: image,
      url: "/assets/svg/rust.svg",
      description: "Rust logo",
    },
    description:
      "The idea of this project is to implement as many machine learning algorithms as possible to better understand how they work and learn more about Rust.",
    technologies: [
      {
        name: "Rust",
        image: "assets/svg/rust.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #5b0900 0%, #070101 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/rust-machine-learning-algorithms",
      repository:
        "https://gitlab.com/Bangal22/rust-machine-learning-algorithms",
    },
  },
  {
    title: "ParseInt-Rust",
    projectType: [Programming],
    media: {
      type: image,
      url: "/assets/svg/rust.svg",
      description: "Rust logo",
    },
    description:
      "This project was created to parse a String value to Integer (i16, i32, i64, i128) in an easy way and also to practice some Rust concepts.",
    technologies: [
      {
        name: "Rust",
        image: "assets/svg/rust.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #5b0900 0%, #070101 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/parseint-rust",
      repository: "https://gitlab.com/Bangal22/parseint-rust",
    },
  },
  {
    title: "Artificial intelligence models",
    projectType: [DataScientist],
    media: {
      type: image,
      url: "/assets/svg/jupyter-notebook.svg",
      description: "Jupyter Notebook logo",
    },
    description:
      "In this repository I will be uploading all my notebooks of the models I will be implementing.",
    technologies: [
      {
        name: "Python",
        image: "assets/svg/python.svg",
      },
      {
        name: "Jupyter Notebook",
        image: "assets/svg/jupyter-notebook.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #f37821 0%, #f37821 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/artificial-intelligence-models",
      repository: "https://gitlab.com/Bangal22/artificial-intelligence-models",
    },
  },
  {
    title: "Blog Articles",
    projectType: [Backend, Frontend],
    media: {
      type: image,
      url: "/assets/svg/php.svg",
      description: "PHP logo",
    },
    description: "CRUD application with php implementing MVC pattern.",
    technologies: [
      {
        name: "PHP",
        image: "assets/svg/php.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #6181b6 0%, #6181b6 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/blog-articles",
      repository: "https://gitlab.com/Bangal22/blog-articles",
    },
  },
  {
    title: "Star Hunters",
    projectType: [Backend, Frontend],
    media: {
      type: image,
      url: "/assets/svg/javascript.svg",
      description: "JavaScript logo",
    },
    description:
      "Real time game implemented with web socket and using MVC pattern",
    technologies: [
      {
        name: "JavaScript",
        image: "assets/svg/javascript.svg",
      },
      {
        name: "Node js",
        image: "assets/svg/nodejs.svg",
      },
    ],
    customColor: {
      colorGradient: "-70deg, #ffd200 0%, #ffd200 100%",
      boxShadow: "0 40px 86px rgba(240, 156, 67, 0.34)",
    },
    urlProjects: {
      web: "https://gitlab.com/Bangal22/StarHunters",
      repository: "https://gitlab.com/Bangal22/StarHunters",
    },
  },
];
