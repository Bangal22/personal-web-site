import { Technology, CustomColor, UrlProjects } from "@interface/index";
import { Media } from "@interface/index";

export enum ProjectType {
  DataEngineering = "Data engineering",
  DataScientist = "Data scientist",
  FullStack = "FullStack",
  Frontend = "Front-end",
  Backend = "Back-end",
  MachineLearning = "Machine Learning",
  DataVisualization = "Data Visualization",
  ProgrammingLogic = "Programming logic",
  Programming = "Programming",
}

export interface Project {
  title: string;
  projectType: ProjectType[];
  media?: Media;
  description: string;
  technologies: Technology[];
  customColor?: CustomColor;
  urlProjects?: UrlProjects;
}
