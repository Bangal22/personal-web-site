export interface Experience {
    name : string, 
    location : string, 
    position : string, 
    img : string, 
    description : string, 
    responsibilities : string[], 
    mainAchievement : string[], 
}
