export enum MediaType {
  video,
  image,
}
export interface Media {
  type: MediaType;
  url: string;
  description: string;
}
