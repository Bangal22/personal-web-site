export interface CustomColor {
  colorGradient: string;
  boxShadow: string;
}
