export interface Icon {
  name: string;
  svg: string;
  classAnchor: string;
  title: string;
  href: string;
}
