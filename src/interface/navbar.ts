export interface NavBar {
  title: string;
  value: string;
  href: string;
  classAnchor: string;
}
